Universe = vanilla
environment = LD_LIBRARY_PATH=./
Executable = CondorWrapper.sh
Error = error/job.$(Process).error
Output = out/job.$(Process).out
Log = log/job.$(Process).log
should_transfer_files = YES
when_to_transfer_output = ON_Exit
transfer_output = True
transfer_input_files = HistCreatorCondorReady.py, AtlasStyle.C, AtlasStyle.h

# ############################# 
Arguments = $(Process) ttbar 0

Queue
Arguments = $(Process) wtaunu 0
Queue

Arguments = $(Process) wtaunu 0
Queue

Arguments = $(Process) wwlvlv 0
Queue

Arguments = $(Process) HPlus90 0
Queue

Arguments = $(Process) HPlus100 0
Queue

Arguments = $(Process) HPlus110 0
Queue

Arguments = $(Process) HPlus120 0
Queue

Arguments = $(Process) HPlus400 0
Queue

# ############################# 
Arguments = $(Process) ttbar 10
Queue

Arguments = $(Process) wtaunu 10
Queue

Arguments = $(Process) wtaunu 10
Queue

Arguments = $(Process) wwlvlv 10
Queue

Arguments = $(Process) HPlus90 10
Queue

Arguments = $(Process) HPlus100 10
Queue

Arguments = $(Process) HPlus110 10
Queue

Arguments = $(Process) HPlus120 10
Queue

Arguments = $(Process) HPlus400 10
Queue

# ############################# 
# Arguments = $(Process) ttbar 0
# Queue

# Arguments = $(Process) HPlus1000 0
# Queue

# Arguments = $(Process) HPlus175 0
# Queue

# Arguments = $(Process) HPlus100 0
# Queue

############################# 
# Arguments = $(Process) ttbar 5
# Queue

# Arguments = $(Process) HPlus1000 5
# Queue

# Arguments = $(Process) HPlus175 5
# Queue

# Arguments = $(Process) HPlus100 5
# Queue

# ############################# 
# Arguments = $(Process) ttbar 10
# Queue

# Arguments = $(Process) HPlus1000 10
# Queue

# Arguments = $(Process) HPlus175 10
# Queue

# Arguments = $(Process) HPlus100 10
# Queue