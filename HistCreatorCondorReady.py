#! /usr/bin/env python
from __future__ import division
# import ROOT
from ROOT import *
from math import pi
import argparse

"""
Need to fill the numhist for each particle.

"""

def ParseArugments():
	parser=argparse.ArgumentParser()
	parser.add_argument("-c", "--condor", type=bool, required=False, dest="Condor_Option", help="Use Condor (Process Number is now required)", default=False)
	parser.add_argument("-p", "--process", type=str, required=False, dest="Process", help="Process Number (To be attached to end of file name)")
	parser.add_argument("-f", "--file", type=str, required=True, dest="input_file", help="Which file to run over.")
	parser.add_argument("-t", "-tree", type=str, required=False, dest="input_tree", help="The name of the tree to access", default="CollectionTree")
	# parser.add_argument("-event", "--eventrange", type=str, required=True, dest="EventRange", help="Choose an event range to run over (A for All, S for Single, R for Range)")
	parser.add_argument("-ptcut", "--ptCut", type=float, required=False, dest="pt_cut", help="Pt cut to apply to all objects", default=0.0)

	args = parser.parse_args()

	if args.Process == "" or args.Process == None:
		process = 0
	else:
		process = args.Process

	if args.input_file.lower() == "ttbar":
		# input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_HIGG6D1.e3698_s2608_s2183_r7725_r7676_p2949/DAOD_HIGG6D1.10336113.*"
		input_file_path = "/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_HIGG6D2.e6337_e5984_s3126_r9364_r9315_p3517/*"
	# elif args.input_file.lower() == "hplus1000":
		# input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc15_13TeV.341535.aMcAtNloPythia8EvtGen_A14NNPDF23LO_Hplus4FS_H1000_taunu.merge.DAOD_HIGG6D1.e4621_a766_a821_r7676_p2949/DAOD_HIGG6D1.10334497.*"
		# input_file_path = "/nfs/pt3nfs/xdata/eparrrish/HPlusTauNu_DxAODs/"
	elif args.input_file.lower() == "hplus90":
		# input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc15_13TeV.344253.MadGraphPythia8EvtGen_A14NNPDF23LO_HplusInt_H175_taunu.merge.DAOD_HIGG6D1.e5115_a766_a821_r7676_p2949/DAOD_HIGG6D1.10335508.*"
		input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.344287.MadGraphPythia8EvtGen_A14NNPDF23LO_Hplus_H90_taunu.deriv.DAOD_HIGG6D2.e5148_e5984_a875_r9364_r9315_p3517/*"
	elif args.input_file.lower() == "hplus100":
		input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.344288.MadGraphPythia8EvtGen_A14NNPDF23LO_Hplus_H100_taunu.deriv.DAOD_HIGG6D2.e5148_e5984_a875_r9364_r9315_p3575/*"
	elif args.input_file.lower() == "hplus110":
		input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.344289.MadGraphPythia8EvtGen_A14NNPDF23LO_Hplus_H110_taunu.deriv.DAOD_HIGG6D2.e5148_e5984_a875_r9364_r9315_p3517/*"
	elif args.input_file.lower() == "hplus120":
		input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.344290.MadGraphPythia8EvtGen_A14NNPDF23LO_Hplus_H120_taunu.deriv.DAOD_HIGG6D2.e5148_e5984_a875_r9364_r9315_p3575/*"
	elif args.input_file.lower() == "hplus400":
		# input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc15_13TeV.344288.MadGraphPythia8EvtGen_A14NNPDF23LO_Hplus_H100_taunu.merge.DAOD_HIGG6D1.e5148_a766_a821_r7676_p2949/DAOD_HIGG6D1.10335192.*"
		input_file_path = "/nfs/pt3nfs/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.341003.aMcAtNloPythia8EvtGen_A14NNPDF23LO_Hplus4FS_H400_taunu.deriv.DAOD_HIGG6D2.e4621_e5984_a875_r9364_r9315_p3517/*"
	elif args.input_file.lower() == "hhbb260":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343438.PwPy8EG_AZNLO_CTEQ6L1_VBF260_lephad_hh_bbtautau.HIGG4D2.Goldcrest2_CxAOD.root/group.phys-higgs.10846945._000001.CxAOD.root"
	elif args.input_file.lower() == "hhbb300":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343439.PwPy8EG_AZNLO_CTEQ6L1_VBF300_lephad_hh_bbtautau.HIGG4D2.Goldcrest2_CxAOD.root/group.phys-higgs.10846954._000001.CxAOD.root"
	elif args.input_file.lower() == "hhbb400":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343440.PwPy8EG_AZNLO_CTEQ6L1_VBF400_lephad_hh_bbtautau.HIGG4D2.Goldcrest3_CxAOD.root/group.phys-higgs.10874430._000001.CxAOD.root"
	elif args.input_file.lower() == "hhbb500":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343441.PwPy8EG_AZNLO_CTEQ6L1_VBF500_lephad_hh_bbtautau.HIGG4D2.Goldcrest2_CxAOD.root/group.phys-higgs.10846999._000001.CxAOD.root"
	elif args.input_file.lower() == "hhbb600":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343442.PwPy8EG_AZNLO_CTEQ6L1_VBF600_lephad_hh_bbtautau.HIGG4D2.Goldcrest2_CxAOD.root/group.phys-higgs.10847021._000002.CxAOD.root"
	elif args.input_file.lower() == "hhbb700":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343443.PwPy8EG_AZNLO_CTEQ6L1_VBF700_lephad_hh_bbtautau.HIGG4D2.Goldcrest2_CxAOD.root/group.phys-higgs.10847046._000001.CxAOD.root"
	elif args.input_file.lower() == "hhbb800":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343444.PwPy8EG_AZNLO_CTEQ6L1_VBF800_lephad_hh_bbtautau.HIGG4D2.Goldcrest3_CxAOD.root/group.phys-higgs.10874436._000001.CxAOD.root"
	elif args.input_file.lower() == "hhbb900":
		input_file_path = "/xdata/eparrish/HHbbtautau_CxAODs/group.phys-higgs.mc15_13TeV.343445.PwPy8EG_AZNLO_CTEQ6L1_VBF900_lephad_hh_bbtautau.HIGG4D2.Goldcrest2_CxAOD.root/group.phys-higgs.10847079._000001.CxAOD.root"
	elif args.input_file.lower() == "wtaunu":
		input_file_path = "/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.deriv.DAOD_HIGG6D2.e5340_s3126_r9364_r9315_p3517/*"
	elif args.input_file.lower() == "wwlvlv":
		input_file_path = "/xdata/eparrish/HPlusTauNu_DxAODs/mc16_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.deriv.DAOD_HIGG6D2.e4616_s3126_r9364_r9315_p3517/*"
	else:
		raise Exception("Invalid File Option")

	# if args.EventRange.lower() == "a":
 #        input_event_range = "ALL"
 #    elif args.EventRange.lower() == "s":
 #        input_event_range = int(raw_input("Select The Event to Run Over "))
 #    elif args.EventRange.lower() == "r":
 #        range_start = int(raw_input("Select The Start of The Event Range "))
 #        range_end = int(raw_input("Select The End of The Event Range "))
 #        input_event_range = xrange(range_start, range_end)
 #    else:
 #        raise Exception("Non Valid Event Range Option")

	inputs = {
		"Sample Type":		args.input_file,
		"File Path": 		input_file_path,
		"Tree Name":	 	args.input_tree,
		# "Event Range": 		input_event_range,
		"Process":			args.Process,
		"Pt Cut":			args.pt_cut,
	} 
	return inputs

def OpenFile(filepath, treename):
	f = TChain(treename)
	f.Add(filepath)
	t = xAOD.MakeTransientTree(f)
	return f, t

def CreateHistos(sampletype, particle, ptcut):

	massPlotBinInfo = { #bins,xmin,xmax
		"TruthTaus": 		[50, 	1.5, 		2],
		"TruthMuons":		[50, 	0.075, 	0.125],
		"TruthPhotons":		[20, 	-0.1, 	0.1],
		"TruthElectrons":	[900, 	0.0001, 0.001],
		"TruthNeutrinos":	[20, 	-0.1, 	0.1],
		"TruthTops":		[201, 	170.995, 173.005], #(172.3-171.7)/60
		"TruthBottoms":		[90,	0.9,	8.1],
		"TruthWs":			[110, 	74.5,	85.5],
		"TruthParticles":	[2001, 	-0.5, 1000.5],
	}

	if sampletype.lower() == "ttbar":
		massPlotBinInfo["TruthTops"] = [310, 157.5, 188.5]

	if sampletype.lower() == "hplus90":
		massPlotBinInfo["TruthHPlus"] =	[300, 	88.5, 	91.5]

	elif sampletype.lower() == "hplus100":
		massPlotBinInfo["TruthHPlus"] =	[300, 	98.5, 	101.5]

	elif sampletype.lower() == "hplus110":
		massPlotBinInfo["TruthHPlus"] =	[300, 	108.5, 	111.5]

	elif sampletype.lower() == "hplus120":
		massPlotBinInfo["TruthHPlus"] =	[300, 	118.5, 	121.5]

	elif sampletype.lower() == "hplus175":
		massPlotBinInfo["TruthHPlus"] =	[300, 	173.5, 	176.5]

	elif sampletype.lower() == "hplus400":
		massPlotBinInfo["TruthHPlus"] =	[300, 	398.5, 	401.5]

	elif sampletype.lower() == "hplus1000":
		massPlotBinInfo["TruthHPlus"] =	[300, 	998.5, 	1001.5]

	else:
		massPlotBinInfo["TruthHPlus"] = [2000, 0, 1000]

	fullpdgidPlotBinInfo = { #bins,xmin,xmax
		"TruthTaus": 		[41, 	-20.5, 	20.5],
		"TruthMuons":		[31, 	-15.5, 	15.5],
		"TruthPhotons":		[51, 	-25.5, 	25.5],
		"TruthElectrons":	[31, 	-15.5, 	15.5],
		"TruthNeutrinos":	[41, 	-20.5, 	20.5],
		"TruthTops":		[21, 	-10.5, 	10.5],
		"TruthBottoms":		[21,	-10.5, 	10.5],
		"TruthParticles":	[81,	-40.5, 	40.5],
		"TruthHPlus":		[81,	-40.5, 	40.5],
		"TruthWs":			[61, 	-30.5,	30.5],
	}

	pthist = TH1D("%spthist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s p_{T} [p_{T} > %s GeV];p_{T}^{%s};Fraction of %s / 5 GeV" %(sampletype, particle, ptcut, particle, particle), 160,0,800)
	etahist = TH1D("%setahist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s #eta [p_{T} > %s GeV];#eta^{%s};Fraction of %s / 0.1" %(sampletype, particle, ptcut, particle, particle), 100,-5,5)
	phihist = TH1D("%sphihist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s #phi [p_{T} > %s GeV];#phi^{%s};Fraction of %s / 0.1" %(sampletype, particle, ptcut, particle, particle), 64, -pi,pi)
	masshist = TH1D("%smasshist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s Mass [p_{T} > %s GeV];mass^{%s};Fraction of %s / %s GeV" %(sampletype, particle, ptcut, particle, particle, (massPlotBinInfo[particle][2] - massPlotBinInfo[particle][1])/float(massPlotBinInfo[particle][0])), massPlotBinInfo[particle][0],massPlotBinInfo[particle][1],massPlotBinInfo[particle][2])
	ehist = TH1D("%sehist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s E [p_{T} > %s GeV];E^{%s};Fraction of %s / 5 GeV" %(sampletype, particle, ptcut, particle, particle), 160,0,800)
	pdgidhist1 = TH1D("%sLowpdgIdhist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s PDGID [p_{T} > %s GeV];pdgId;Fraction of Particles"%(sampletype, particle, ptcut), 21,-0.5,20.5)
	pdgidhist2 = TH1D("%sMiddlepdgIdhist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s PDGID [p_{T} > %s GeV];pdgId;Fraction of Particles"%(sampletype, particle, ptcut), 31,19.5,50.5)
	pdgidhist3 = TH1D("%sHighpdgIdhist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s PDGID [p_{T} > %s GeV];pdgId;Fraction of Particles"%(sampletype, particle, ptcut), 1951,49.5,2000.5)
	pdgidhist4 = TH1D("%sFullpdgIdhist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s PDGID [p_{T} > %s GeV];pdgId;Fraction of Particles"%(sampletype, particle, ptcut), fullpdgidPlotBinInfo[particle][0],fullpdgidPlotBinInfo[particle][1],fullpdgidPlotBinInfo[particle][2])
	parentpdgidhist = TH1D("%sParentpdgIdhist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s Parents PDGID [p_{T} > %s GeV];pdgId;Fraction of Particles"%(sampletype, particle, ptcut), 101,-0.5,100.5)
	numhist = TH1D("%sNumhist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s Number of %s [p_{T} > %s GeV];Number of %s;Number of Events"%(sampletype, particle, ptcut, particle), 51, -0.5, 50.5)
	nparents = TH1D("%sParentNumhist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s Number of Parents of %s [p_{T} > %s GeV];Number of Parents of %s;Number of Events"%(sampletype, particle, ptcut, particle), 101, -0.5, 100.5)
	detahist = TH1D("%sdetahist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s max(#Delta#eta) [p_{T} > %s GeV];max(#delta#eta^{%s});Fraction of %s / 0.1" %(sampletype, particle, ptcut, particle, particle), 202,-10.1,10.1)
	dphihist = TH1D("%sdphihist%s_ptCut_%s"%(particle, sampletype, ptcut), "%s %s max(#Delta#phi) [p_{T} > %s GeV];max(#delta#phi^{%s});Fraction of %s / 0.1" %(sampletype, particle, ptcut, particle, particle), 128,-2*pi,2*pi)

	theseHistos = {
		"pt":			pthist,
		"eta":			etahist,
		"phi":			phihist,
		"mass":			masshist,
		"E":			ehist,
		"pdgId1":		pdgidhist1,
		"pdgId2":		pdgidhist2,
		"pdgId3":		pdgidhist3,
		"pdgId4":		pdgidhist4,
		"parentpdgId":	parentpdgidhist,
		"num":			numhist,
		"dEta":			detahist,
		"dPhi":			dphihist,
	}

	if sampletype.lower() == "hplus90":
		for hist in theseHistos:
			theseHistos[hist].SetLineColor(kBlack)
			theseHistos[hist].SetLineWidth(2)

	elif sampletype.lower() == "hplus100":
		for hist in theseHistos:
			theseHistos[hist].SetLineColor(kBlue)
			theseHistos[hist].SetLineWidth(2)
			

	elif sampletype.lower() == "hplus110":
		for hist in theseHistos:
			theseHistos[hist].SetLineColor(kGreen+2)
			theseHistos[hist].SetLineWidth(2)

	elif sampletype.lower() == "hplus120":
		for hist in theseHistos:
			theseHistos[hist].SetLineColor(kOrange+2)
			theseHistos[hist].SetLineWidth(2)		

	elif sampletype.lower() == "hplus400":
		for hist in theseHistos:
			theseHistos[hist].SetLineColor(kMagenta)
			theseHistos[hist].SetLineWidth(2)	

	elif sampletype.lower() == "ttbar":
		for hist in theseHistos:
			theseHistos[hist].SetLineColor(kRed)
			theseHistos[hist].SetLineWidth(2)

	else:
		for hist in theseHistos:
			theseHistos[hist].SetLineColor(kCyan+1)
			theseHistos[hist].SetLineWidth(2)

	return theseHistos

def FillHistos(f, t, ptcut, histostoFill):
	# SR Cuts (Look at tau_lep_categories.py for full description of cuts)

	# class SR_tauel(Category):
	#     name = "SR_tauel"
	#     label = "tau+el signal"
	#     tau_id = TauID
	#     regionFFIDX = 7
	#     with_sys = True
	#     cuts = (
	#         CLEAN_EVT +
	#         tauel_base +
	#         met_base +
	#         jet_base +
	#         TCut("n_bjets>=1"))

	# class SR_taumu(Category):
	#     name = "SR_taumu"
	#     label = "tau+mu signal"
	#     tau_id = TauID
	#     regionFFIDX = 7
	#     with_sys = True
	#     cuts = (
	#         CLEAN_EVT +
	#         taumu_base +
	#         met_base +
	#         jet_base +
	#         TCut("n_bjets>=1"))

	# is2015 = TCut("NOMINAL_pileup_random_run_number<288000")
	# is2016 = TCut("NOMINAL_pileup_random_run_number>=288000")
	# CLEAN_EVT = TCut("(event_clean==1) && (n_vx>=1) && (bsm_tj_dirty_jet==0)")
	# tauel_base = HLT_EL+tau_base+el_base+TCut("el_0_q*tau_0_q==-1")+TCut("n_muons==0")
	# taumu_base = HLT_MU+tau_base+mu_base+TCut("mu_0_q*tau_0_q==-1")+TCut("n_electrons==0")
	# hlt_el_2015 = TCut("el_0_trig_HLT_e24_lhmedium_L1EM20VH || el_0_trig_HLT_e60_lhmedium || el_0_trig_HLT_e120_lhloose")
	# hlt_el_2016 = TCut("el_0_trig_HLT_e26_lhtight_nod0_ivarloose || el_0_trig_HLT_e60_lhmedium_nod0 || el_0_trig_HLT_e140_lhloose_nod0")
	# tau_base = TCut("tau_0_pt>30000&&abs(tau_0_eta)<2.3&&!(abs(tau_0_eta)<1.52&&abs(tau_0_eta)>1.37)")
	# HLT_EL = TCut((is2015*hlt_el_2015).GetTitle()+"+"+(is2016*hlt_el_2016).GetTitle())
	# met_base= TCut("met_et>50000")
	# jet_base= TCut("n_jets>=1&&jet_0_pt>25000")

	pdgIds = {
	"TruthTaus": 		15,
	"TruthMuons":		13,
	"TruthPhotons":		22,
	"TruthElectrons":	11,
	"TruthNeutrinos":	[12,14,16],
	"TruthTops":		6,
	"TruthHPlus":		37,
	"TruthBottoms":		5,
	"TruthWs":			24,
	}


	nentries = t.GetEntries()
	for event in xrange(nentries):
		t.GetEntry(event)
		if event % 1000 == 0:
			print "  Processing Events %s-%s of %s" %(event, event+1000, nentries)
		else:
			pass
		neutrinoSum = TLorentzVector()
		numTruthParticles = 0
		numTruthTaus = 0
		numTruthMuons = 0
		numTruthPhotons = 0
		numTruthElectrons = 0
		numTruthTops = 0
		numTruthHPlus = 0
		numTruthNeutrinos = 0
		numTruthWs = 0

		dEtaHPlus = [-999]
		dPhiHPlus = [-999]

		for thisparticle in t.TruthParticles:
			if thisparticle.pt()/1000.00 < ptcut:
				continue
			# if thisparticle.status() not in [21, 22, 23, 24]:
			# 	continue

			else:
				histostoFill["TruthParticles"]["pt"].Fill(thisparticle.pt()/1000.00)
				histostoFill["TruthParticles"]["eta"].Fill(thisparticle.eta())
				histostoFill["TruthParticles"]["phi"].Fill(thisparticle.phi())
				histostoFill["TruthParticles"]["E"].Fill(thisparticle.e()/1000.00)
				histostoFill["TruthParticles"]["pdgId1"].Fill(thisparticle.absPdgId())
				histostoFill["TruthParticles"]["pdgId2"].Fill(thisparticle.absPdgId())
				histostoFill["TruthParticles"]["pdgId3"].Fill(thisparticle.absPdgId())
				histostoFill["TruthParticles"]["pdgId4"].Fill(thisparticle.pdgId())
				numTruthParticles += 1
				# print thisparticle.status()
				try:
					histostoFill["TruthParticles"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
				except:
					pass

				particleVector = TLorentzVector()
				particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
				histostoFill["TruthParticles"]["mass"].Fill(particleVector.M())

				if thisparticle.absPdgId() == pdgIds["TruthTaus"]:
					histostoFill["TruthTaus"]["pt"].Fill(thisparticle.pt()/1000.00)
					histostoFill["TruthTaus"]["eta"].Fill(thisparticle.eta())
					histostoFill["TruthTaus"]["phi"].Fill(thisparticle.phi())
					histostoFill["TruthTaus"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthTaus"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthTaus"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthTaus"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthTaus"]["pdgId4"].Fill(thisparticle.pdgId())
					numTruthTaus += 1
					try:
						histostoFill["TruthTaus"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass

					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthTaus"]["mass"].Fill(particleVector.M())

				elif thisparticle.absPdgId() == pdgIds["TruthMuons"]:
					histostoFill["TruthMuons"]["pt"].Fill(thisparticle.pt()/1000.00)
					histostoFill["TruthMuons"]["eta"].Fill(thisparticle.eta())
					histostoFill["TruthMuons"]["phi"].Fill(thisparticle.phi())
					histostoFill["TruthMuons"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthMuons"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthMuons"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthMuons"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthMuons"]["pdgId4"].Fill(thisparticle.pdgId())
					numTruthMuons += 1
					try:
						histostoFill["TruthMuons"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass

					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthMuons"]["mass"].Fill(particleVector.M())

				elif thisparticle.absPdgId() == pdgIds["TruthPhotons"]:
					histostoFill["TruthPhotons"]["pt"].Fill(thisparticle.pt()/1000.00)
					histostoFill["TruthPhotons"]["eta"].Fill(thisparticle.eta())
					histostoFill["TruthPhotons"]["phi"].Fill(thisparticle.phi())
					histostoFill["TruthPhotons"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthPhotons"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthPhotons"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthPhotons"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthPhotons"]["pdgId4"].Fill(thisparticle.pdgId())
					numTruthPhotons += 1
					try:
						histostoFill["TruthPhotons"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass

					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthPhotons"]["mass"].Fill(particleVector.M())

				elif thisparticle.absPdgId() == pdgIds["TruthElectrons"]:
					histostoFill["TruthElectrons"]["pt"].Fill(thisparticle.pt()/1000.00)
					histostoFill["TruthElectrons"]["eta"].Fill(thisparticle.eta())
					histostoFill["TruthElectrons"]["phi"].Fill(thisparticle.phi())
					histostoFill["TruthElectrons"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthElectrons"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthElectrons"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthElectrons"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthElectrons"]["pdgId4"].Fill(thisparticle.pdgId())
					numTruthElectrons += 1
					try:
						histostoFill["TruthElectrons"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass

					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthElectrons"]["mass"].Fill(particleVector.M())

				elif thisparticle.absPdgId() == pdgIds["TruthTops"]:
					histostoFill["TruthTops"]["pt"].Fill(thisparticle.pt()/1000.00)
					histostoFill["TruthTops"]["eta"].Fill(thisparticle.eta())
					histostoFill["TruthTops"]["phi"].Fill(thisparticle.phi())
					histostoFill["TruthTops"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthTops"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthTops"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthTops"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthTops"]["pdgId4"].Fill(thisparticle.pdgId())
					numTruthTops += 1
					try:
						histostoFill["TruthTops"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass

					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthTops"]["mass"].Fill(particleVector.M())
					# print "Truth Top Mass: %s" %(particleVector.M())

				elif thisparticle.absPdgId() == pdgIds["TruthBottoms"]:
					histostoFill["TruthBottoms"]["pt"].Fill(thisparticle.pt()/1000.00)
					histostoFill["TruthBottoms"]["eta"].Fill(thisparticle.eta())
					histostoFill["TruthBottoms"]["phi"].Fill(thisparticle.phi())
					histostoFill["TruthBottoms"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthBottoms"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthBottoms"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthBottoms"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthBottoms"]["pdgId4"].Fill(thisparticle.pdgId())
					numTruthTops += 1
					try:
						histostoFill["TruthBottoms"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass

					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthBottoms"]["mass"].Fill(particleVector.M())

				elif thisparticle.absPdgId() == pdgIds["TruthHPlus"]:
					histostoFill["TruthHPlus"]["pt"].Fill(thisparticle.pt()/1000.00)
					histostoFill["TruthHPlus"]["eta"].Fill(thisparticle.eta())
					histostoFill["TruthHPlus"]["phi"].Fill(thisparticle.phi())
					histostoFill["TruthHPlus"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthHPlus"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthHPlus"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthHPlus"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthHPlus"]["pdgId4"].Fill(thisparticle.pdgId())
					if numTruthHPlus == 0:
						pass
						lastHPlus = thisparticle
					else:
						# print "Hello"
						dEtaHPlus.append(thisparticle.eta() - lastHPlus.eta())
						dPhiHPlus.append(thisparticle.phi() - lastHPlus.phi())
					numTruthHPlus += 1
					try:
						histostoFill["TruthHPlus"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass

					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthHPlus"]["mass"].Fill(particleVector.M())

				elif thisparticle.absPdgId() in pdgIds["TruthNeutrinos"]:
					histostoFill["TruthNeutrinos"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthNeutrinos"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthNeutrinos"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthNeutrinos"]["pdgId4"].Fill(thisparticle.pdgId())
					histostoFill["TruthNeutrinos"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthNeutrinos"]["eta"].Fill(thisparticle.eta())
					numTruthNeutrinos += 1
					try:
						histostoFill["TruthNeutrinos"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass
					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthNeutrinos"]["mass"].Fill(particleVector.M())
					neutrinoSum += particleVector

				elif thisparticle.absPdgId() == pdgIds["TruthWs"]:
					histostoFill["TruthWs"]["pdgId1"].Fill(thisparticle.absPdgId())
					histostoFill["TruthWs"]["pdgId2"].Fill(thisparticle.absPdgId())
					histostoFill["TruthWs"]["pdgId3"].Fill(thisparticle.absPdgId())
					histostoFill["TruthWs"]["pdgId4"].Fill(thisparticle.pdgId())
					histostoFill["TruthWs"]["E"].Fill(thisparticle.e()/1000.00)
					histostoFill["TruthWs"]["eta"].Fill(thisparticle.eta())
					numTruthWs += 1
					try:
						histostoFill["TruthWs"]["parentpdgId"].Fill(thisparticle.parent().absPdgId())
					except:
						pass
					particleVector = TLorentzVector()
					particleVector.SetPtEtaPhiE(thisparticle.pt()/1000.00,thisparticle.eta(),thisparticle.phi(), thisparticle.e()/1000.00)
					histostoFill["TruthWs"]["mass"].Fill(particleVector.M())

				else:
					continue

		neutrinoSum = -neutrinoSum
		if neutrinoSum.Pt()!= 0: 
			histostoFill["TruthNeutrinos"]["pt"].Fill(neutrinoSum.Pt())
			# histostoFill["TruthNeutrinos"]["eta"].Fill(neutrinoSum.Eta())
			histostoFill["TruthNeutrinos"]["phi"].Fill(neutrinoSum.Phi())
			# histostoFill["TruthNeutrinos"]["E"].Fill(neutrinoSum.E())
			# histostoFill["TruthNeutrinos"]["mass"].Fill(neutrinoSum.M())
		else:
			pass

		histostoFill["TruthParticles"]["num"].Fill(numTruthParticles)
		histostoFill["TruthTaus"]["num"].Fill(numTruthTaus)
		histostoFill["TruthMuons"]["num"].Fill(numTruthMuons)
		histostoFill["TruthPhotons"]["num"].Fill(numTruthPhotons)
		histostoFill["TruthElectrons"]["num"].Fill(numTruthElectrons)
		histostoFill["TruthTops"]["num"].Fill(numTruthTops)
		histostoFill["TruthHPlus"]["num"].Fill(numTruthHPlus)
		histostoFill["TruthNeutrinos"]["num"].Fill(numTruthNeutrinos)
		histostoFill["TruthWs"]["num"].Fill(numTruthWs)


		histostoFill["TruthHPlus"]["dEta"].Fill(max(dEtaHPlus))
		histostoFill["TruthHPlus"]["dPhi"].Fill(max(dPhiHPlus))

		# t.Draw("lengths$(TruthParticles)>>%s"%(histostoFill["TruthParticles"]["num"].GetName()), "", "")

	# print "Particle PDGID: %s" %(thisparticle.absPdgId())

	return

def PlotOnSameCanvas(FilePath, treename, ptcut, sampletype, process):


	thisfile = OpenFile(FilePath, treename)

	AllHistos = {
		"TruthTaus": 		CreateHistos(sampletype, "TruthTaus", ptcut),
		"TruthMuons": 		CreateHistos(sampletype, "TruthMuons", ptcut),
		"TruthPhotons": 	CreateHistos(sampletype, "TruthPhotons", ptcut),
		"TruthElectrons": 	CreateHistos(sampletype, "TruthElectrons", ptcut),
		"TruthNeutrinos": 	CreateHistos(sampletype, "TruthNeutrinos", ptcut),
		"TruthParticles": 	CreateHistos(sampletype, "TruthParticles", ptcut),
		"TruthTops": 		CreateHistos(sampletype, "TruthTops", ptcut),
		"TruthHPlus":		CreateHistos(sampletype, "TruthHPlus", ptcut),
		"TruthBottoms":		CreateHistos(sampletype, "TruthBottoms", ptcut),
		"TruthWs":			CreateHistos(sampletype, "TruthWs", ptcut),
	}

	print "Filling Histograms"
	FillHistos(thisfile[0], thisfile[1], ptcut, AllHistos)

	print "Plotting Histograms"

	for particle in AllHistos:
		outfile = TFile("%s_%s_pt_%s.root"%(sampletype,particle,ptcut), "RECREATE")
		for variable in AllHistos[particle]:
			thiscanvas = TCanvas()
			AllHistos[particle][variable].Draw()
			AllHistos[particle][variable].SetMaximum(float(AllHistos[particle][variable].GetMaximum())+0.05)
			AllHistos[particle][variable].SetMinimum(0)
			AllHistos[particle][variable].Fill(AllHistos[particle][variable].GetBinCenter(AllHistos[particle][variable].GetNbinsX()+1), AllHistos[particle][variable].GetBinContent(AllHistos[particle][variable].GetNbinsX()+1))
			AllHistos[particle][variable].Fill(AllHistos[particle][variable].GetBinLowEdge(1)-1, AllHistos[particle][variable].GetBinContent(0))
			# AllHistos[particle][variable].GetXaxis().SetLabelSize(0.02)
			try:
				AllHistos[particle][variable].Scale(1/AllHistos[particle][variable].Integral())
			except:
				pass
			AllHistos[particle][variable].Write()
			thiscanvas.Update()
			# thiscanvas.SaveAs("%s_%s%s_pt_%s.root"%(sampletype, particle, variable, ptcut))
		outfile.Write()
		outfile.Close()
	print "Successfully Created ROOT Files"
	return


if __name__ == "__main__":
	gROOT.SetBatch(True)
	gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
	xAOD.Init()
	gROOT.LoadMacro("AtlasStyle.C")
	SetAtlasStyle()
	# gStyle.SetOptStat(0)

	inputs = ParseArugments()

	# PlotOnSameCanvas("TruthTaus", hplus1000File, hplus1000Tree, hplus175File, hplus175Tree, hplus100File, hplus100Tree, ttbarFile, ttbarTree, ptcut)
	# PlotOnSameCanvas("TruthMuons", hplus1000File, hplus1000Tree, hplus175File, hplus175Tree, hplus100File, hplus100Tree, ttbarFile, ttbarTree, ptcut)
	# PlotOnSameCanvas("TruthPhotons", hplus1000File, hplus1000Tree, hplus175File, hplus175Tree, hplus100File, hplus100Tree, ttbarFile, ttbarTree, ptcut)
	# PlotOnSameCanvas("TruthElectrons", hplus1000File, hplus1000Tree, hplus175File, hplus175Tree, hplus100File, hplus100Tree, ttbarFile, ttbarTree, ptcut)
	# PlotOnSameCanvas("TruthNeutrinos", hplus1000File, hplus1000Tree, hplus175File, hplus175Tree, hplus100File, hplus100Tree, ttbarFile, ttbarTree, ptcut)
	PlotOnSameCanvas(inputs["File Path"], inputs["Tree Name"], inputs["Pt Cut"], inputs["Sample Type"], inputs["Process"])