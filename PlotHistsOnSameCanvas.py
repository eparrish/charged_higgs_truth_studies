import ROOT
import argparse

def ParseArugments():
	parser=argparse.ArgumentParser()
	parser.add_argument("-ptcut", "--ptCut", type=float, required=True, dest="pt_cut", help="Pt cut to apply to all objects", default=0.0)
	parser.add_argument("-log", "--logScale", action="store_true", required=False, dest="log_scale", help="Use log scale", default=False)
	args = parser.parse_args()

	inputs = {
		"Pt Cut":			args.pt_cut,
		"Log Scale":		args.log_scale,
	} 
	return inputs

def GrabHistos(filepath, particletype, sampletype, ptcut):
	thisfile = ROOT.TFile(filepath, "READ")
	# thisfile.Print()
	# thisfile.ls()
	if thisfile.IsZombie():
		raise Exception("This file is a zombie. %s" %(filepath))
	else:
		# ROOT.gROOT.cd()
		listofHistoNames = [
			"%sMiddlepdgIdhist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sHighpdgIdhist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sphihist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%setahist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%smasshist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sFullpdgIdhist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%spthist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sParentpdgIdhist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sehist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sLowpdgIdhist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sNumhist%s_ptCut_%s" %(particletype, sampletype, ptcut),
			"%sdetahist%s_ptCut_%s"%(particletype, sampletype, ptcut),
			"%sdphihist%s_ptCut_%s"%(particletype, sampletype, ptcut),
		]

		# MiddleHistpdgIdHist = ROOT.TH1D()
		# MiddleHistpdgIdHistTEMP = thisfile.Get(listofHistoNames[0])
		# MiddleHistpdgIdHist = MiddleHistpdgIdHistTEMP.Clone()

		GrabbedHistos = {
			"MiddlepdgId":	thisfile.Get(listofHistoNames[0]).Clone(),
			"HighpdgId":	thisfile.Get(listofHistoNames[1]).Clone(),
			"phi":			thisfile.Get(listofHistoNames[2]).Clone(),
			"eta":			thisfile.Get(listofHistoNames[3]).Clone(),
			"mass":			thisfile.Get(listofHistoNames[4]).Clone(),
			"FullpdgId":	thisfile.Get(listofHistoNames[5]).Clone(),
			"pt":			thisfile.Get(listofHistoNames[6]).Clone(),
			"ParentpdgId":	thisfile.Get(listofHistoNames[7]).Clone(),
			"e":			thisfile.Get(listofHistoNames[8]).Clone(),
			"LowpdgId":		thisfile.Get(listofHistoNames[9]).Clone(),
			"num":			thisfile.Get(listofHistoNames[10]).Clone(),
			"dEta":			thisfile.Get(listofHistoNames[11]).Clone(),
			"dPhi":			thisfile.Get(listofHistoNames[12]).Clone(),
		}
		for hist in GrabbedHistos.keys():
			GrabbedHistos[hist].SetDirectory(0)

	# print "\nIn GrabHistos: \n \t %s" %GrabbedHistos
	return GrabbedHistos

def Main(pt_cut, log):

	ParticleTypes = [
		"TruthTaus",
		"TruthMuons",
		"TruthPhotons",
		"TruthElectrons",
		"TruthNeutrinos",
		"TruthParticles",
		"TruthTops",
		"TruthBottoms",
		"TruthHPlus",
		"TruthWs"
	]

	SampleTypes = [
		# "HPlus1000",
		# "HPlus175",
		# "HPlus100",
		"HPlus90",
		"HPlus100",
		"HPlus110",
		"HPlus120",
		"HPlus400",
		"ttbar",
		"wtaunu",
		"wwlvlv",
	]

	AllTheHistosByFile = {}

	# newFile = ROOT.TFile("Test.root", "RECREATE")
	# ROOT.gROOT.cd()
	for stype in SampleTypes:
		thisSampleHistos = {}

		for particle in ParticleTypes:
			thisSampleHistos[particle] = GrabHistos("%s_%s_pt_%s.root" %(stype, particle, pt_cut), particle, stype, pt_cut)

		AllTheHistosByFile[stype] = thisSampleHistos
		# print "\nIn Sample Loop: \n \t %s" %thisSampleHistos

	# print "\nAt End: \n \t %s" %AllTheHistosByFile

	for particle in AllTheHistosByFile["HPlus100"]:
		for variable in AllTheHistosByFile["HPlus100"][particle]:
			# thisStack = ROOT.THStack()
			thiscanvas = ROOT.TCanvas()
			thiscanvas.Divide(4,2)
			i = 1
			maxOfHistosList = []
			for sample in AllTheHistosByFile:

				# print AllTheHistosByFile[sample][particle][variable]
				# thisStack.Add(AllTheHistosByFile[sample][particle][variable])
				thiscanvas.cd(i)
				if log:
					try:
						if AllTheHistosByFile[sample][particle][variable].Scale(1/AllTheHistosByFile[sample][particle][variable].Integral()) !=0:
							try:
								ROOT.gPad.SetLogy()
							except:
								#AllTheHistosByFile[sample][particle][variable].SetMinimum(0)
								pass
						else:
							AllTheHistosByFile[sample][particle][variable].SetMinimum(0)
					except:
						AllTheHistosByFile[sample][particle][variable].SetMinimum(0)
				else:
					AllTheHistosByFile[sample][particle][variable].SetMinimum(0)

				AllTheHistosByFile[sample][particle][variable].Draw("hist")
				# AllTheHistosByFile[sample][particle][variable].SetTitle(AllTheHistosByFile[sample][particle][variable].GetName())
				# AllTheHistosByFile[sample][particle][variable].GetXaxis()SetTitle(AllTheHistosByFile[sample][particle][variable].GetXaxis().GetTitle())
				# AllTheHistosByFile[sample][particle][variable].GetYaxis()SetTitle(AllTheHistosByFile[sample][particle][variable].GetYaxis().GetTitle())
				# AllTheHistosByFile[sample][particle][variable].SetMaximum(float(AllTheHistosByFile[sample][particle][variable].GetMaximum())+0.05)
				# AllTheHistosByFile[sample][particle][variable].SetMinimum(0)
				i += 1

				try:
					AllTheHistosByFile[sample][particle][variable].Scale(1/AllTheHistosByFile[sample][particle][variable].Integral())
				except:
					pass
				maxOfHistosList.append(AllTheHistosByFile[sample][particle][variable].GetMaximum())
			# thiscanvas = ROOT.TCanvas()
			# thisStack.Draw("NOSTACK")
			# thisStack.GetXaxis().SetTitle(AllTheHistosByFile[sample][particle][variable].GetXaxis().GetTitle())
			# thisStack.GetYaxis().SetTitle(AllTheHistosByFile[sample][particle][variable].GetYaxis().GetTitle())
			# thiscanvas.BuildLegend()
			# thisStack.SetMaximum(float(AllTheHistosByFile[sample][particle][variable].GetMaximum())+0.05)
			# thisStack.SetMinimum(0)
			# thiscanvas.SaveAs("%s%s_pt_%s.png"%(particle,variable, ptcut))
			maxofHistos = max(maxOfHistosList)
			# print maxofHistos, maxOfHistosList
			for sample in AllTheHistosByFile:
				AllTheHistosByFile[sample][particle][variable].SetMaximum(float(maxofHistos)+0.01)
				# AllTheHistosByFile[sample][particle][variable].GetXaxis().SetLabelSize(0.04)
				# AllTheHistosByFile[sample][particle][variable].SetMinimum(0)

			thiscanvas.Update()
			thiscanvas.SaveAs("%s%s_pt_%s.png"%(particle,variable, pt_cut))
			# raw_input("Press Enter to Continue")



	return

if __name__ == "__main__":
	ROOT.gROOT.SetBatch(True)
	myInputs = ParseArugments()
	Main(myInputs["Pt Cut"], myInputs["Log Scale"])