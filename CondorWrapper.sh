#! /bin/bash

export Home=$(pwd)

#ROOT
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

rcSetup -u; rcSetup Base,2.4.28
rc find_packages
rc compile

printf "Start time: "; /bin/date
printf "Job is running on node: "; /bin/hostname
printf "Job is running in directory: "; /bin/pwd

python HistCreatorCondorReady.py -p ${1} -f ${2} -ptcut ${3}